package ru.yar.bird.remindme;

public class Constans {

    public static final int TAB_ONE = 0;
    public static final int TAB_TWO = 1;
    public static final int TAB_THREE = 2;

    public static class URL {
        private static final String HOST = "http://193.233.48.58:8080";

        public static final String GET_REMIND_ITEM = HOST + "reminder/get";
    }
}
